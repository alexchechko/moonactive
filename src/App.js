import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import CustomTabs from "./components/Tabs";
import Grid from '@material-ui/core/Grid';
import {HistoricalRatesTabs} from "./components/HistoricalRates/HistoricalRatesTabs";
import DailyPredefinedRates from "./components/DailyPredifinedRates";
import CurrencyConverter from "./components/CurrencyConverter";
import {Header} from "./components/Header";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
        textAlign: 'center'
    },
    container: {
        padding: theme.spacing(2),
        maxWidth: 1440,
        textAlign: 'center',
        margin: '0 auto'
    }
}));

function App() {

    const classes = useStyles();

    const tabs = [
        {"label": "Currency Converter", "component": <CurrencyConverter/>},
        {"label": "Historical Rates", "component": <HistoricalRatesTabs/>}
    ];

    return (
        <Grid className={classes.root}>
            <Header
                title={"Alex Chechko - Exchange Rate Application"}
                className={classes.title}
            />
            <Grid
                container
                spacing={3}
                className={classes.container}
            >
                <Grid item lg={8}>
                    <CustomTabs tabs={tabs}/>
                </Grid>
                <Grid item lg={4}>
                    <DailyPredefinedRates/>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default App;
