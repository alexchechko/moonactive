// Actions
export const UPDATE_EXCHANGE_RATES = 'UPDATE_EXCHANGE_RATES';
export const UPDATE_EXCHANGE_RATES_HISTORY = 'UPDATE_EXCHANGE_RATES_HISTORY';
export const CHANGE_FROM_VALUE = 'CHANGE_FROM_VALUE';
export const SET_BASE_CURRENCY = 'SET_BASE_CURRENCY';
export const SET_TARGET_CURRENCY = 'SET_TARGET_CURRENCY';


// APIs
export const EXCHANGE_RATE_API_ENDPOINT = 'https://api.exchangeratesapi.io';

// Precision
export const AMOUNT_PRECISION = 2;
export const RATE_PRECISION = 4;
export const AXIS_LABEL_PRECISION = 3;


