export const getMin = (data, prop) => {
    return data.reduce((min, p) => p[prop] < min ? p[prop] : min, data[0][prop]);
};

export const getMax = (data, prop) => {
    return data.reduce((max, p) => p[prop] > max ? p[prop] : max, data[0][prop]);
};