import React from "react";
import ReactCountryFlag from "react-country-flag"

export const FlagIcon = (props) => {
    return (
        <ReactCountryFlag
            countryCode={props.countryCode}
            svg
            style={{
                width: '2em',
                height: '2em',
            }}
        />
    )
};

// todo: propTypes