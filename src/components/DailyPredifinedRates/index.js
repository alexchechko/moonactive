import React from "react";
import {connect} from "react-redux";
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import {Header} from "../Header";
import {DailyRateItem} from "./DailyRateItem";


class ConnectedCurrentDayRates extends React.Component {

    render() {

        const {baseCurrency, targetCurrency, rates} = {...this.props};

        const ILS = <DailyRateItem countryCode={"IL"} currency={"ILS"} rate={rates["ILS"]}/>;
        const USD = <DailyRateItem countryCode={"US"} currency={"USD"} rate={rates["USD"]}/>;
        const EUR = <DailyRateItem countryCode={"EU"} currency={"EUR"} rate={rates["EUR"]}/>;
        const GBP = <DailyRateItem countryCode={"GB"} currency={"GBP"} rate={rates["GBP"]}/>;
        const CAD = <DailyRateItem countryCode={"CA"} currency={"CAD"} rate={rates["CAD"]}/>;
        const MXN = <DailyRateItem countryCode={"MX"} currency={"MXN"} rate={rates["MXN"]}/>;
        const JPY = <DailyRateItem countryCode={"JP"} currency={"JPY"} rate={rates["JPY"]}/>;


        const ILSNotSet = [baseCurrency, targetCurrency].indexOf('ILS') === -1;

        return (
            <div>
                <Header
                    title={"Today's Rates"}
                    color={"default"}
                />
                <div>
                    <Typography component="div">
                        <Box p={3} boxShadow={2}>
                            <p style={{textAlign: 'start'}}>
                                1 {`${baseCurrency}`} =
                            </p>

                            {ILSNotSet && ILS}
                            {baseCurrency === "EUR" ? USD : EUR}
                            {baseCurrency === "GBP" ? USD : GBP}
                            {baseCurrency === "CAD" ? USD : CAD}
                            {baseCurrency === "MXN" ? USD : MXN}
                            {baseCurrency === "JPY" ? USD : JPY}
                        </Box>
                    </Typography>
                </div>

            </div>
        )
    }
}

// todo: propTypes

const mapStateToProps = state => {
    return {
        baseCurrency: state.baseCurrency,
        targetCurrency: state.targetCurrency,
        rates: state.rates,
    }
};


const DailyPredefinedRates = connect(mapStateToProps, null)(ConnectedCurrentDayRates);
export default DailyPredefinedRates;