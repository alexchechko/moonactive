import {FlagIcon} from "../../FlagIcon";
import React from "react";

export const DailyRateItem = (props) => {
    return (
        <div style={{
            display: "flex",
            justifyContent: "space-around",
            margin: "0.8rem"
        }}>
            <FlagIcon countryCode={props.countryCode}/>
            <div>{props.currency}</div>
            <div>{props.rate}</div>
        </div>
    );
};

// todo: propTypes