import React from 'react';
import TextField from '@material-ui/core/TextField';

export const CustomInput = (props) => {

    const {label, onChange, value} = {...props};

    return (
        <TextField
            id="outlined-number"
            label={label}
            type="number"
            InputProps={{
                style: {
                    padding: '0.423rem',
                    marginRight: '5px'
                },
            }}
            onChange={onChange}
            variant="outlined"
            value={value}
        />
    )
};

// todo: propTypes



