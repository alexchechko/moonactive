import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

function TabPanel(props) {
    const {children, value, index, ...other} = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            {...other}
        >
            {value === index && <Box p={3} boxShadow={2}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
});

class CustomTabs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            value: 0
        }
    }

    handleChange = (event, value) => {
        this.setState({value: value});
    };

    render() {
        const {classes, tabs} = {...this.props};

        return (
            <div className={classes.root}>
                <AppBar
                    position="static"
                    color={'default'}>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}>
                        {tabs.map((tab, index) =>
                            <Tab
                                key={index}
                                label={tab.label}
                                id={index}
                            />
                        )}
                    </Tabs>
                </AppBar>
                {tabs.map((tab, index) =>
                    <TabPanel
                        key={index}
                        value={this.state.value}
                        index={index}>

                        {tab.component}

                    </TabPanel>
                )}
            </div>
        );
    }

}

export default withStyles(styles)(CustomTabs);
