import React from "react";
import * as moment from "moment";
import {connect} from "react-redux";
import Grid from '@material-ui/core/Grid';


class ConnectedRateDetails extends React.Component {
    render() {
        const {baseCurrency, targetCurrency, rates, lastUpdated} = {...this.props};

        return (
            <Grid
                container
                style={{
                    textAlign: "left",
                    padding: '1rem'}
                }>
                <Grid item xs={12}>
                    Your Rate:
                </Grid>
                <Grid
                    item
                    xs={12}
                    style={{
                        fontWeight: 600,
                        fontSize: '1.2rem'
                    }}>
                    1 {`${baseCurrency} = ${rates[targetCurrency]} ${targetCurrency}`}
                </Grid>
                <Grid item xs={12}>
                    Last updated {`${moment(lastUpdated).format('YYYY MMM D')}`}
                </Grid>
            </Grid>
        )
    };
};

// todo: propTypes

const mapStateToProps = state => {
    return {
        baseCurrency: state.baseCurrency,
        targetCurrency: state.targetCurrency,
        rates: state.rates,
        lastUpdated: state.lastUpdated,
    }
};


const RateDetails = connect(mapStateToProps, null)(ConnectedRateDetails);
export default RateDetails;
