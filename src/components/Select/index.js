import React from "react";
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import {CurrencySelectItem} from "./CurrencySelectItem";

export const CustomSelect = (props) => {
    return (
        <Select
            value={props.value}
            onChange={props.onSelect}
            variant={'outlined'}
        >
            {props.options.map((entry, index) => {
                return (
                    <MenuItem value={entry.currency} key={index}>
                        <CurrencySelectItem value={entry.currency} country={entry.country}/>
                    </MenuItem>
                )
            })}
        </Select>
    )
};