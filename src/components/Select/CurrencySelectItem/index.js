import React from 'react';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Grid from '@material-ui/core/Grid';
import {FlagIcon} from "../../FlagIcon";


export const CurrencySelectItem = (props) => {
    return (
        <Grid container>
            <ListItemIcon>
                <FlagIcon countryCode={props.country}/>
            </ListItemIcon>
            <ListItemText primary={props.value}/>
        </Grid>
    )
};

