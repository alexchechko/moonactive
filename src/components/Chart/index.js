import React from "react";
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';

export const Chart = (props) => {

    const {data, xAxis, yAxis, range, title} = {...props};

    return (
        <div>
            <div style={{textAlign: 'center'}}>{title}</div>
            <LineChart
                width={800}
                height={300}
                data={data}
                margin={{top: 10}}
            >
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey={xAxis}/>
                <YAxis domain={range}/>
                <Tooltip/>
                <Line
                    type="linear"
                    dataKey={yAxis}
                    stroke="#8884d8"
                    dot={false}
                />
            </LineChart>
        </div>
    )
};

// todo: propTypes