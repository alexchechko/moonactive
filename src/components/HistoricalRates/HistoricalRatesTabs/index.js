import React from 'react';
import CustomTabs from "../../Tabs";
import HistoricalRates from "../index";

export const HistoricalRatesTabs = (props) => {
    const {base, target} = {props};

    const tabs = [
        {
            "label": "Last Month",
            "component": <HistoricalRates base={base} target={target} offset={1}/>
        },
        {
            "label": "Last 3 Months",
            "component": <HistoricalRates base={base} target={target} offset={3}/>
        },
        {
            "label": "Last 6 Months",
            "component": <HistoricalRates base={base} target={target} offset={6}/>
        },
        {
            "label": "Last 12 Months",
            "component": <HistoricalRates base={base} target={target} offset={12}/>
        }
    ];

    return (
        <CustomTabs
            color={'default'}
            tabs={tabs}
        />
    )
};

// todo: propTypes








