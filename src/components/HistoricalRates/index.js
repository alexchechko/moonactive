import React from 'react';
import * as moment from "moment";
import {
    fetchExchangeRatesHistory,
} from "../../actions/ExchangeRates";
import {connect} from "react-redux";
import _ from 'lodash';
import {Chart} from "../Chart";

class ConnectedHistoricalRates extends React.Component {

    componentDidMount() {

        const dateFormat = 'YYYY-MM-DD';

        const {
            offset,
            baseCurrency,
            targetCurrency,
            history,
            fetchExchangeRatesHistory
        } = {...this.props};

        const offsetId = this.getOffsetId(offset);
        const startDate = moment().subtract(offset, 'months').format(dateFormat);
        const endDate = moment().format(dateFormat);

        if (_.isEmpty(history[offsetId])) {
            fetchExchangeRatesHistory(baseCurrency, targetCurrency, startDate, endDate, offset)
                .then(() => console.log('success'))
                .catch(error => console.log(error))
        }
    }

    getOffsetId = (offset) => {
        return `last_${offset}`
    };

    getTitle = () => {
        const {baseCurrency, targetCurrency} = {...this.props};

        return `${baseCurrency} vs ${targetCurrency}`
    };

    render() {
        const {history, range, offset} = {...this.props};
        const title = this.getTitle();
        const offsetId = this.getOffsetId(offset);
        return (
            <Chart
                data={history[offsetId]}
                range={range}
                xAxis={'date'}
                yAxis={'rate'}
                title={title}
            />
        )
    }

}

// todo: propTypes

const mapStateToProps = state => {
    return {
        baseCurrency: state.baseCurrency,
        targetCurrency: state.targetCurrency,
        history: state.history,
        range: state.range
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchExchangeRatesHistory: (base, target, startDate, endDate, offset) =>
            dispatch(fetchExchangeRatesHistory(base, target, startDate, endDate, offset)),
    };
};

const HistoricalRates = connect(mapStateToProps, mapDispatchToProps)(ConnectedHistoricalRates);

export default HistoricalRates






