import React from 'react';
import {CURRENCIES} from '../../constants/Currencies'
import {CustomSelect} from "../Select";
import {setBaseCurrency, fetchExchangeRates, setTargetCurrency, changeFromValue} from "../../actions/ExchangeRates";
import {connect} from "react-redux";
import RateDetails from "../RateDetails";
import _ from 'lodash';
import {CustomInput} from "../Input";
import Grid from '@material-ui/core/Grid';

class ConnectedConverter extends React.Component {

    componentDidMount() {
        const {
            baseCurrency,
            fetchExchangeRates,
            rates
        } = {...this.props};

        if (_.isEmpty(rates)) {
            fetchExchangeRates(baseCurrency)
                .then(() => console.log("success"))
                .catch((error) => console.log(error))
        }
    }

    handleChange = (event) => {
        const value = event.target.value;

        this.props.changeFromValue(value)
    };

    selectBaseCurrency = (e) => {
        const baseCurrency = e.target.value;

        const {
            setBaseCurrency,
            fetchExchangeRates
        } = {...this.props};

        setBaseCurrency(baseCurrency)
            .then(() => {
                fetchExchangeRates(baseCurrency)
                    .then(() => console.log("success"))
                    .catch((error) => console.log(error)
                    )
            });
    };

    selectTargetCurrency = (e) => {
        const targetCurrency = e.target.value;

        this.props.setTargetCurrency(targetCurrency);
    };


    render() {

        const {fromValue, toValue, baseCurrency, targetCurrency} = {...this.props};

        return (
            <div>
                <Grid container spacing={3}>
                    <Grid item>
                        <CustomInput
                            label={"From"}
                            onChange={this.handleChange}
                            value={fromValue}
                        />
                        <CustomSelect
                            value={baseCurrency}
                            onSelect={this.selectBaseCurrency}
                            options={CURRENCIES}
                        />
                    </Grid>
                    <Grid item>
                        <CustomInput
                            label={"To"}
                            readOnly
                            value={toValue}
                        />
                        <CustomSelect
                            value={targetCurrency}
                            onSelect={this.selectTargetCurrency}
                            options={CURRENCIES}
                        />
                    </Grid>
                    <RateDetails/>
                </Grid>
            </div>
        );
    }
}

// todo: prop types

const mapStateToProps = state => {
    return {
        baseCurrency: state.baseCurrency,
        targetCurrency: state.targetCurrency,
        rates: state.rates,
        toValue: state.toValue,
        fromValue: state.fromValue,
        lastUpdated: state.lastUpdated,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchExchangeRates: (base) => dispatch(fetchExchangeRates(base)),
        setBaseCurrency: (currency) => dispatch(setBaseCurrency(currency)),
        setTargetCurrency: (currency) => dispatch(setTargetCurrency(currency)),
        changeFromValue: (value) => dispatch(changeFromValue(value))
    };
};

const CurrencyConverter = connect(mapStateToProps, mapDispatchToProps)(ConnectedConverter);

export default CurrencyConverter






