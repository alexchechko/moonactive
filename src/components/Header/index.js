import React from "react";
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';

export const Header = (props) => {
    return (
        <AppBar position="static" color={props.color}>
            <Toolbar>
                <Typography variant={"h6"} {...props}>
                    {props.title}
                </Typography>
            </Toolbar>
        </AppBar>
    )
};

// todo: propTypes