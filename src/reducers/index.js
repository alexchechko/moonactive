import _ from 'lodash'
import {
    UPDATE_EXCHANGE_RATES_HISTORY,
    UPDATE_EXCHANGE_RATES,
    CHANGE_FROM_VALUE,
    SET_BASE_CURRENCY,
    SET_TARGET_CURRENCY,
    RATE_PRECISION,
    AMOUNT_PRECISION,
    AXIS_LABEL_PRECISION
} from "../constants";

import {getMax, getMin} from "../utils/Object";

const initialState = {
    toValue: 0,
    fromValue: 1000,
    baseCurrency: "USD",
    targetCurrency: "ILS",
    rates: [],
    lastUpdated: "",
    history: {}
};

function rootReducer(state = initialState, action) {
    switch (action.type) {
        case UPDATE_EXCHANGE_RATES:

            const data = action.payload;

            return {
                ...state,
                rates: _.mapValues(data.rates, value => _.round(value, RATE_PRECISION)),
                lastUpdated: data.date,
                toValue: _.round(state.fromValue * data.rates[state.targetCurrency], AMOUNT_PRECISION)
            };

        case UPDATE_EXCHANGE_RATES_HISTORY:

            let result = [];

            _.forOwn(action.payload.rates, (value, key) => result.push({
                'date': key,
                'rate': _.round(value[state.targetCurrency], RATE_PRECISION)
            }));

            result = _.orderBy(result, ['date'], ['asc']);

            const minValue = _.round(getMin(result, 'rate') * 0.98, AXIS_LABEL_PRECISION);
            const maxValue = _.round(getMax(result, 'rate') * 1.02, AXIS_LABEL_PRECISION);

            return {
                ...state,
                history: {
                    ...state.history,
                    [`last_${action.offset}`]: result
                },
                range: [minValue, maxValue]
            };

        case CHANGE_FROM_VALUE:

            const fromValue = _.round(action.fromValue, 2);

            return {
                ...state,
                fromValue: fromValue,
                toValue: _.round(fromValue * state.rates[state.targetCurrency], AMOUNT_PRECISION)
            };

        case SET_BASE_CURRENCY:
            return {
                ...state,
                baseCurrency: action.baseCurrency,
                history: {}
            };

        case SET_TARGET_CURRENCY:

            const targetCurrency = action.targetCurrency;
            return {
                ...state,
                targetCurrency: targetCurrency,
                toValue: _.round(state.fromValue * state.rates[targetCurrency], AMOUNT_PRECISION),
                history: {}
            };

        default:
            return {...state}

    }
};

export default rootReducer;


