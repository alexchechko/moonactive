import axios from "axios";
import {
    SET_BASE_CURRENCY, CHANGE_FROM_VALUE, SET_TARGET_CURRENCY,
    EXCHANGE_RATE_API_ENDPOINT,
    UPDATE_EXCHANGE_RATES,
    UPDATE_EXCHANGE_RATES_HISTORY
} from "../constants";

const http = axios.create({
    baseURL: EXCHANGE_RATE_API_ENDPOINT
});

// Exchange Rates
export const updateExchangeRates = (data) => {
    return {
        type: UPDATE_EXCHANGE_RATES,
        payload: data
    }
};

export const fetchExchangeRates = (base) => {
    return (dispatch) => {
        return http.get("/latest", { //todo: extract to http util
            params: {base: base},
        })
            .then(response => {
                if (response.status === 200) {
                    return response;
                } else {
                    throw response;
                }
            })
            .then(json => {
                const jsonResponse = json.data;

                dispatch(updateExchangeRates(jsonResponse));
            })
            .catch(error => {
                throw error;
            });
    }
};

// Historical Rates
export const updateExchangeRatesHistory = (offset, data) => {
    return {
        type: UPDATE_EXCHANGE_RATES_HISTORY,
        payload: data,
        offset: offset
    }
};

export const fetchExchangeRatesHistory = (base, target, startDate, endDate, offset) => {
    return (dispatch) => {
        return http.get("/history", { //todo: extract to http util
            params: {
                base: base,
                symbols: target,
                start_at: startDate,
                end_at: endDate
            },
        })
            .then(response => {
                if (response.status === 200) {
                    return response;
                } else {
                    throw response;
                }
            })
            .then(json => {
                const jsonResponse = json.data;

                dispatch(updateExchangeRatesHistory(offset, jsonResponse));
            })
            .catch(error => {
                throw error;
            });
    }
};

// Set base currency
export const updateBaseCurrency = (currency) => {
    return {
        type: SET_BASE_CURRENCY,
        baseCurrency: currency
    }
};

export const setBaseCurrency = (currency) => {
    return (dispatch) => {
        dispatch(updateBaseCurrency(currency));

        return Promise.resolve();
    }
};

// Set target currency
export const updateTargetCurrency = (currency) => {
    return {
        type: SET_TARGET_CURRENCY,
        targetCurrency: currency
    }
};

export const setTargetCurrency = (currency) => {
    return (dispatch) => {
        dispatch(updateTargetCurrency(currency));

        return Promise.resolve();
    }
};

// Control fromValue input
export const updateFromValue = (value) => {
    return {
        type: CHANGE_FROM_VALUE,
        fromValue: value
    }
};

export const changeFromValue = (value) => {
    return (dispatch) => dispatch(updateFromValue(value))
};