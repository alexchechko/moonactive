
## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm start`

Runs the app in the development mode.

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.

You will also see any lint errors in the console.

## Implementation Notes: 
1. Due to limited time there are some inline styles and no use of propTypes for the most part
2. Issues I didn't address: 

* There is a little bug I in the API you provided: when select EUR as both base and target currencies "EUR" rate is not provided in the response (it's ok for all other currencies)

* input validation - negative numbers, strings etc. I just used input type='number' 